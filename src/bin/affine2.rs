mod foo {
    pub struct Token1(());

    pub struct Token2(());

    pub fn start() -> Token1 {
        Token1(())
    }

    pub fn step1(_token: Token1) -> Token2 {
        Token2(())
    }

    pub fn step2(_token: Token2) {}
}

fn main() {
    let token = foo::start();
    let token = foo::step1(token);
    foo::step2(token);

    // let token = foo::Token1(());
}
