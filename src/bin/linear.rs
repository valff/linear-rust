#![deny(unused_variables, unused_assignments, unused_must_use)]

mod foo {
    #[derive(Debug)]
    pub struct Token(());

    #[must_use]
    pub fn start() -> Token {
        Token(())
    }

    pub fn finish(_token: Token) {}
}

fn main() {
    {
        foo::start();
    }
    // {
    //     let token = foo::start();
    //     dbg!(token);
    // }
    // {
    //     let _token = foo::start();
    // }
}
