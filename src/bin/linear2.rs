#![deny(unused_variables, unused_assignments, unused_must_use)]

mod foo {
    #[derive(Debug)]
    pub struct Token(());

    #[must_use]
    pub fn start() -> Token {
        Token(())
    }

    pub fn finish(_token: Token) {}

    impl Drop for Token {
        fn drop(&mut self) {
            panic!("token must be used");
        }
    }
}

fn main() {
    {
        let _token = foo::start();
    }
    // {
    //     let token = foo::start();
    //     std::mem::forget(token);
    // }
    // {
    //     use std::{cell::RefCell, rc::Rc};

    //     struct Node {
    //         token: Option<foo::Token>,
    //         next: Option<Rc<RefCell<Node>>>,
    //     }

    //     let token = foo::start();
    //     let node1 = Rc::new(RefCell::new(Node {
    //         token: Some(token),
    //         next: None,
    //     }));
    //     let node2 = Rc::new(RefCell::new(Node {
    //         token: None,
    //         next: Some(Rc::clone(&node1)),
    //     }));
    //     node1.borrow_mut().next = Some(node2);
    // }
}
