mod token {
    pub struct Token(());

    impl Token {
        /// Obtain a token.
        ///
        /// # Safety
        ///
        /// The caller must ensure that only one token instance is created in
        /// the whole program run-time.
        pub unsafe fn new() -> Self {
            Self(())
        }
    }
}

fn initialize(_token: token::Token) {}

fn main() {
    let token = unsafe { token::Token::new() };
    initialize(token);
    // initialize(token);
}
