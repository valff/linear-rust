// #[derive(Clone, Copy)]
struct Token;

// impl Clone for Token {
//     fn clone(&self) -> Self {
//         Token
//     }
// }

// impl Copy for Token {}

fn operation(_token: Token) {}

fn main() {
    let token = Token;
    operation(token);
    // operation(token);
}
